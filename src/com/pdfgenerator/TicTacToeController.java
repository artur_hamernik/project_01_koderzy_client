package com.pdfgenerator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Vector;
import javax.swing.JButton;

import static com.pdfgenerator.TicTacToeServices.*;

public class TicTacToeController
{
    private final String  STATUS_X_START        = "X moves to start the game";
    private final String  STATUS_O_START        = "O moves to start the game";
    private final String  STATUS_DRAW         = "It's a draw!";
    private final String  STATUS_P_X_WINS       = "Player playing X wins the game!";
    private final String  STATUS_AI_X_WINS       = "AI playing X wins the game!";
    private final String  STATUS_P_O_WINS       = "Player playing O wins the game!";
    private final String  STATUS_AI_O_WINS       = "AI playing O wins the game!";
    private final String  STATUS_X_MOVES      = "X to move";
    private final String  STATUS_O_MOVES      = "O to move";
    private final String  STATUS_CP_MOVES     = "Computer is thinking...";
    private final String PLAYER_X_AI_O = "Player is X, AI is O";
    private final String PLAYER_O_AI_X = "Player is O, AI is X";
    private final char  MARK_X  = 'X';
    private final char  MARK_O  = 'O';
    private char  PLAYER_MARK = MARK_X;
    private char AI_MARK = MARK_O;
    private boolean blockMove = false;

    private TicTacToeView  view;

    public TicTacToeController( TicTacToeView view)
    {
        this.view = view;
        this.view.addGameBoardSquareButtonListener( new SquareListener() );
        this.view.addGameBoardSquareButtonHoverListener( new SquareHoverListener() );
        this.view.addNewGameButtonListener( new NewGameListener() );
        this.view.addMarkButtonListener( new MarkListener() );
        resetHttpClient();
    }

    private class SquareListener implements ActionListener
    {
        @Override
        public void actionPerformed( ActionEvent e )
        {
            if ( !blockMove ) {
                String gameStatus;
                JButton square = (JButton) e.getSource();
                int row = Integer.parseInt(square.getClientProperty("row").toString());
                int col = Integer.parseInt(square.getClientProperty("col").toString());
                if(gameIsComplete()) return;
                if(squareHasBeenPlayed(row, col)) return;
                setFieldHttpClient(row, col, PLAYER_MARK);

                gameStatus = STATUS_CP_MOVES;

                if(gameIsComplete()) {
                    if (isWin()){
                        if (PLAYER_MARK == 'X')
                            gameStatus = STATUS_P_X_WINS;
                        else
                            gameStatus = STATUS_P_O_WINS;
                    } else if (isWinAi()) {
                        if (AI_MARK == 'X')
                            gameStatus = STATUS_AI_X_WINS;
                        else
                            gameStatus = STATUS_AI_O_WINS;
                    } else if (isDraw()) {
                        gameStatus = STATUS_DRAW;
                    }
                }

                view.updateGameStatusLabelText( gameStatus );
                resultHttpClient();

                Vector<Vector<String>> tab = getMatrix();
                view.updateGameBoardUI( tab );
                performWinLineUpdates();

                if(!gameIsComplete()) {
                    computerMove();
                }

            }
        }

        private void computerMove()
        {
            blockMove = true;
            setFieldAiHttpClient(AI_MARK);
            java.util.Timer timer = new java.util.Timer();
            timer.schedule( new java.util.TimerTask() {
                @Override
                public void run() {
                    String gameStatus;
                    if(PLAYER_MARK == 'X')
                        gameStatus = STATUS_X_MOVES;
                    else
                        gameStatus = STATUS_O_MOVES;
                    if(gameIsComplete()){
                        if(isDraw()) gameStatus = STATUS_DRAW;
                        if(isWinAi()){
                            if(AI_MARK == 'O') gameStatus = STATUS_AI_O_WINS;
                            if(AI_MARK == 'X') gameStatus = STATUS_AI_X_WINS;
                        }
                    }
                    view.updateGameStatusLabelText( gameStatus );
                    resultHttpClient();

                    Vector<Vector<String>> tab = getMatrix();
                    view.updateGameBoardUI( tab );
                    performWinLineUpdates();
                    blockMove = false;
                }
            }, 750 );

        }

        private void performWinLineUpdates()
        {
            if ( isWin() || isWinAi() ) {
                int row1 = getStartRow();
                int col1 = getStartCol();
                int row2 = getEndRow();
                int col2 = getEndCol();
                view.updateWinnerLine( row1, col1, row2, col2 );
            }
        }

    }

    private class SquareHoverListener extends MouseAdapter {

        @Override
        public void mouseEntered( MouseEvent e ) {
            JButton square = (JButton) e.getSource();
            int row = Integer.parseInt(square.getClientProperty("row").toString());
            int col = Integer.parseInt(square.getClientProperty("col").toString());

            if ( !gameIsComplete() && squareHasBeenPlayed(row, col)) {
                view.updateSquareUIForHoverState( row, col );
            }
        }

        @Override
        public void mouseExited( MouseEvent e ) {
            JButton square = (JButton) e.getSource();
            int row = Integer.parseInt(square.getClientProperty("row").toString());
            int col = Integer.parseInt(square.getClientProperty("col").toString());
            view.updateSquareUIForNormalState( row, col );
        }

    }

    class NewGameListener implements ActionListener
    {
        @Override
        public void actionPerformed( ActionEvent e )
        {
            resetHttpClient();
            view.resetWinnerLine();
            resultHttpClient();

            Vector<Vector<String>> tab = getMatrix();
            view.updateGameBoardUI( tab );
            if(PLAYER_MARK == 'X')
                view.updateGameStatusLabelText( STATUS_X_START );
            else
                view.updateGameStatusLabelText( STATUS_O_START );
        }


    }

    class MarkListener implements ActionListener
    {
        @Override
        public void actionPerformed( ActionEvent e )
        {
            if ( PLAYER_MARK =='X' ) {
                PLAYER_MARK = 'O';
                AI_MARK = 'X';
                view.updateMarkLabelText( PLAYER_O_AI_X );
                view.updateGameStatusLabelText(STATUS_O_START);
            }
            else if ( PLAYER_MARK == 'O' ) {
                PLAYER_MARK = 'X';
                AI_MARK = 'O';
                view.updateMarkLabelText( PLAYER_X_AI_O );
                view.updateGameStatusLabelText(STATUS_X_START);
            }

            resetHttpClient();
            resultHttpClient();
            Vector<Vector<String>> tab = getMatrix();

            view.resetWinnerLine();
            view.updateGameBoardUI( tab );
        }
    }
}