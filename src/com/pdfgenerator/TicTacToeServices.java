package com.pdfgenerator;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.pdfgenerator.model.SetFieldAIData;
import com.pdfgenerator.model.SetFieldData;
import com.pdfgenerator.model.TicTacToeBoardData;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.Vector;

class TicTacToeServices {
    static boolean isWin() {
        return win;
    }

    static boolean isWinAi() {
        return winAi;
    }

    static boolean isDraw() {
        return draw;
    }

    private static boolean win = false;
    private static boolean winAi = false;
    private static boolean draw = false;
    private static int StartRow;
    private static int StartCol;
    private static int EndRow;
    private static int EndCol;


    static int getStartRow() {
        return StartRow;
    }

    static int getStartCol() {
        return StartCol;
    }

    static int getEndRow() {
        return EndRow;
    }

    static int getEndCol() {
        return EndCol;
    }

    private static Vector<Vector<String>> matrix;

    static Vector<Vector<String>> getMatrix() {
        return matrix;
    }

    private static void checkForDraw(TicTacToeBoardData file){
        Vector<Vector<String>> temp = file.getMatrix();
        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++){
                if(temp.get(i).get(j) != null && temp.get(i).get(j).equals(" ")){
                    draw = false;
                    return;
                }
            }
        }
        if(!win && !winAi){
            draw = true;
        }
    }
    private static void checkForWin(TicTacToeBoardData temp, char mark, char markAi){
        Vector<Vector<String>> board = temp.getMatrix();

        for(int i = 0; i < 3; i++){
            if(board.get(i).get(0).equals(String.valueOf(mark)) && board.get(i).get(1).equals(String.valueOf(mark)) && board.get(i).get(2).equals(String.valueOf(mark))){
                StartRow = i;
                StartCol = 0;
                EndRow = i;
                EndCol = 2;
                win = true;
                return;
            }else if(board.get(0).get(i).equals(String.valueOf(mark)) && board.get(1).get(i).equals(String.valueOf(mark)) && board.get(2).get(i).equals(String.valueOf(mark))){
                StartRow = 0;
                StartCol = i;
                EndRow = 2;
                EndCol = i;
                win = true;
                return;
            }
            else if(board.get(0).get(0).equals(String.valueOf(mark)) && board.get(1).get(1).equals(String.valueOf(mark)) && board.get(2).get(2).equals(String.valueOf(mark))){
                StartRow = 0;
                StartCol = 0;
                EndRow = 2;
                EndCol = 2;
                win = true;
                return;
            }
            else if(board.get(0).get(2).equals(String.valueOf(mark)) && board.get(1).get(1).equals(String.valueOf(mark)) && board.get(2).get(0).equals(String.valueOf(mark))){
                StartRow = 0;
                StartCol = 2;
                EndRow = 2;
                EndCol = 0;
                win = true;
                return;
            }
            else if(board.get(i).get(0).equals(String.valueOf(markAi)) && board.get(i).get(1).equals(String.valueOf(markAi)) && board.get(i).get(2).equals(String.valueOf(markAi))){
                StartRow = i;
                StartCol = 0;
                EndRow = i;
                EndCol = 2;
                winAi = true;
                return;
            }else if(board.get(0).get(i).equals(String.valueOf(markAi)) && board.get(1).get(i).equals(String.valueOf(markAi)) && board.get(2).get(i).equals(String.valueOf(markAi))){
                StartRow = 0;
                StartCol = i;
                EndRow = 2;
                EndCol = i;
                winAi = true;
                return;
            }
            else if(board.get(0).get(0).equals(String.valueOf(markAi)) && board.get(1).get(1).equals(String.valueOf(markAi)) && board.get(2).get(2).equals(String.valueOf(markAi))){
                StartRow = 0;
                StartCol = 0;
                EndRow = 2;
                EndCol = 2;
                winAi = true;
                return;
            }
            else if(board.get(0).get(2).equals(String.valueOf(markAi)) && board.get(1).get(1).equals(String.valueOf(markAi)) && board.get(2).get(0).equals(String.valueOf(markAi))){
                StartRow = 0;
                StartCol = 2;
                EndRow = 2;
                EndCol = 0;
                winAi = true;
                return;
            }
        }
    }
    static void resetHttpClient(){
        final CloseableHttpClient client = HttpClients.createDefault();
        final HttpPost httpPost = new HttpPost("http://127.0.0.1:8080/api/tictactoe/reset-game");

        try {
            final CloseableHttpResponse response = client.execute(httpPost);

            System.out.println("Kod odpowiedzi serwera: " + response.getStatusLine().getStatusCode());

            if(response.getStatusLine().getStatusCode() == 404) {

                System.out.println("Could not restart the game!");
            } else if(response.getStatusLine().getStatusCode() == 200) {
                win = false;
                winAi = false;
                draw = false;
                System.out.println("Game has been successfully restarted!");
            }
            client.close();
        } catch (UnsupportedEncodingException e) {
            System.out.println("Houston, we have a problem with POST unsupported encoding");
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            System.out.println("Houston, we have a problem with POST client protocol");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Houston, we have a problem with POST input output");
            e.printStackTrace();
        }
    }
    static void resultHttpClient(){
        final HttpClient client = HttpClientBuilder.create().build();
        final HttpGet request = new HttpGet("http://127.0.0.1:8080/api/tictactoe/result");
        final Gson gson = new Gson();
        try {

            final HttpResponse response = client.execute(request);
            final HttpEntity entity = response.getEntity();
            final String json = EntityUtils.toString(entity);

            System.out.println("Odczytujemy JSON'a z serwera:");
            System.out.println(json);

            final Type type = new TypeToken<TicTacToeBoardData>(){}.getType();
            final TicTacToeBoardData file = gson.fromJson(json, type);

            System.out.println("Kod odpowiedzi serwera: " + response.getStatusLine().getStatusCode());

            if(response.getStatusLine().getStatusCode() == 404) {
                System.out.println("Brak danych do wyswietlenia!");
            } else if(response.getStatusLine().getStatusCode() == 200) {
                System.out.println(file.toString());
                matrix = file.getMatrix();
            }

        } catch (IOException e) {

            System.out.println("Houston, we have a problem with GET");
            e.printStackTrace();
        }
    }
    static boolean squareHasBeenPlayed(int row, int col){
        resultHttpClient();
        return !matrix.get(row).get(col).equals(" ");
    }
    static boolean gameIsComplete(){
        return isWin() || isWinAi() || isDraw();
    }
    private static void resultForWin(char mark, char markAi){
        final HttpClient client = HttpClientBuilder.create().build();
        final HttpGet request = new HttpGet("http://127.0.0.1:8080/api/tictactoe/result");
        final Gson gson = new Gson();
        try {

            final HttpResponse response = client.execute(request);
            final HttpEntity entity = response.getEntity();
            final String json = EntityUtils.toString(entity);

            final Type type = new TypeToken<TicTacToeBoardData>(){}.getType();
            final TicTacToeBoardData file = gson.fromJson(json, type);

            if(response.getStatusLine().getStatusCode() == 404) {
                System.out.println("Brak danych do wyswietlenia!");
            } else if(response.getStatusLine().getStatusCode() == 200) {

                checkForWin(file, mark, markAi);
                checkForDraw(file);

            }

        } catch (IOException e) {

            System.out.println("Houston, we have a problem with GET");
            e.printStackTrace();
        }
    }
    static void setFieldHttpClient(int fieldX, int fieldY, char mark) {

        final CloseableHttpClient client = HttpClients.createDefault();
        final HttpPost httpPost = new HttpPost("http://127.0.0.1:8080/api/tictactoe/set-field-by-user");

        Gson gson = new Gson();

        // Tworzymy obiekt setField
        final SetFieldData setFieldData = new SetFieldData(fieldX, fieldY, mark);
        // Serializacja obiektu do JSONa
        final String json = gson.toJson(setFieldData);

        try {

            final StringEntity entity = new StringEntity(json);
            httpPost.setEntity(entity);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            final CloseableHttpResponse response = client.execute(httpPost);

            System.out.println("Kod odpowiedzi serwera: " + response.getStatusLine().getStatusCode());

            if(response.getStatusLine().getStatusCode() == 400) {

                System.out.println("Position you want to choose is already marked!");
            } else if(response.getStatusLine().getStatusCode() == 201) {
                System.out.println("Position marked successfully!");
                char markAi;
                if(mark == 'X')
                    markAi = 'O';
                else
                    markAi = 'X';
                resultForWin(mark, markAi);
                if(win){
                    System.out.println("Player won game!");
                }
                else if(draw){
                    System.out.println("Draw!");
                }
            }
            client.close();
        } catch (UnsupportedEncodingException e) {

            System.out.println("Houston, we have a problem with POST unsupported encoding");
            e.printStackTrace();
        } catch (ClientProtocolException e) {

            System.out.println("Houston, we have a problem with POST client protocol");
            e.printStackTrace();
        } catch (IOException e) {

            System.out.println("Houston, we have a problem with POST input output");
            e.printStackTrace();
        }
    }
    static void setFieldAiHttpClient(char markAi) {

        final CloseableHttpClient client = HttpClients.createDefault();
        final HttpPost httpPost = new HttpPost("http://127.0.0.1:8080/api/tictactoe/set-field-by-ai");

        Gson gson = new Gson();

        // Tworzymy obiekt setField
        final SetFieldAIData setFieldAiData = new SetFieldAIData(markAi);
        // Serializacja obiektu do JSONa
        final String json = gson.toJson(setFieldAiData);

        try {

            final StringEntity entity = new StringEntity(json);
            httpPost.setEntity(entity);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            final CloseableHttpResponse response = client.execute(httpPost);

            System.out.println("Kod odpowiedzi serwera: " + response.getStatusLine().getStatusCode());

            if(response.getStatusLine().getStatusCode() == 400) {

                System.out.println("Board is full, game is over!");
            } else if(response.getStatusLine().getStatusCode() == 201) {
                System.out.println("Position marked by AI successfully");
                char mark;
                if(markAi == 'O')
                    mark = 'X';
                else
                    mark = 'O';
                resultForWin(mark, markAi);
                if(winAi){
                    System.out.println("Ai won a game!");
                }
                else if(draw){
                    System.out.println("Draw!");
                }
            }
            client.close();
        } catch (UnsupportedEncodingException e) {

            System.out.println("Houston, we have a problem with POST unsupported encoding");
            e.printStackTrace();
        } catch (ClientProtocolException e) {

            System.out.println("Houston, we have a problem with POST client protocol");
            e.printStackTrace();
        } catch (IOException e) {

            System.out.println("Houston, we have a problem with POST input output");
            e.printStackTrace();
        }
    }
}