package com.pdfgenerator.model;

import java.io.Serializable;

public class SetFieldData implements Serializable {
    private int fieldX;
    private int fieldY;
    private char mark;

    public SetFieldData(int fieldX, int fieldY, char mark) {
        this.fieldX = fieldX;
        this.fieldY = fieldY;
        this.mark = mark;
    }

    public int getFieldX() {
        return fieldX;
    }

    public int getFieldY() {
        return fieldY;
    }

    public char getMark() {
        return mark;
    }
}
