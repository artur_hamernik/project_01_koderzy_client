package com.pdfgenerator;

public class Main {

    public static void main( String[] args )
    {
        TicTacToeView view = new TicTacToeView();
        TicTacToeController controller = new TicTacToeController( view );
        view.setVisible( true );
    }
}
